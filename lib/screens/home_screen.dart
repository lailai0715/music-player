import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  AudioPlayer _audioPlayer = AudioPlayer();
  AudioCache _audioCachePlayer;
  int _counter = 0;
  double _musicProgress = 0.0; //Max: 1.0, Min: 0.0
  bool isPlaying = false;
  bool isStartPlaying = false;
  Duration _musicDuration;
  int time = 0;
  String errMsg;

  Future<int> getMusicTotalDuration() async {
    print("Getting time...");
    int t = await _audioPlayer?.getDuration() ?? 0;
    print("TIME IS $t");
    return t;
  }

  @override
  void initState() {
    super.initState();
    _audioCachePlayer = AudioCache(fixedPlayer: _audioPlayer);
    _audioPlayer.onAudioPositionChanged.listen((Duration duration) async {
      _musicDuration = duration;
      if (time != null && time > 0) {
        _musicProgress = (duration.inMilliseconds / time);
        if (duration.inMilliseconds >= time) {
          isStartPlaying = false;
        }
      } else if (time <= 0) {
        time = await getMusicTotalDuration();
      }

      setState(() {});
    });
    _audioPlayer.onPlayerStateChanged
        .listen((AudioPlayerState audioPlayerState) {
      if (audioPlayerState == AudioPlayerState.PLAYING) {
        isPlaying = true;
      } else {
        isPlaying = false;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    _audioPlayer.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MUSIC PLAYER"),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(isPlaying ? " PLAYING..." : "Stop/Pause"),
              LinearProgressIndicator(
                value: _musicProgress,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                      icon: Icon(isPlaying
                          ? Icons.pause_circle_outline
                          : Icons.play_circle_outline),
                      onPressed: () async {
                        if (isPlaying) {
                          _audioPlayer.pause();
                        } else {
                          await playMusic();
                        }
                      }),
                  IconButton(
                      icon: Icon(Icons.stop),
                      onPressed: () async {
                        await _audioPlayer.stop();
                        isStartPlaying = false;
                        time = 0;
                        _musicProgress = 0.0;
                        _musicDuration = Duration();
                        setState(() {});
                      }),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                  "${_musicDuration?.toString()?.substring(0, 7)} / ${time / 1000}s"),
              SizedBox(
                height: 10,
              ),
              // Text("$errMsg"),
            ],
          ),
        ),
      ),
    );
  }

  Future playMusic() async {
    try {
      if (!isStartPlaying) {
        _audioPlayer = await _audioCachePlayer.play("songs/music.mp3");
        isStartPlaying = true;
      } else {
        await _audioPlayer.resume();
      }
    } catch (e) {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: Text("Error on playing music"),
                content: Text("${e.toString()}"),
              ));
    }
  }
}
